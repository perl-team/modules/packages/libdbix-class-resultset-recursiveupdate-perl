Source: libdbix-class-resultset-recursiveupdate-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libcarp-clan-perl <!nocheck>,
                     libdata-dumper-concise-perl <!nocheck>,
                     libdatetime-perl <!nocheck>,
                     libdbd-sqlite3-perl <!nocheck>,
                     libdbix-class-introspectablem2m-perl <!nocheck>,
                     libdbix-class-perl <!nocheck>,
                     libmoose-perl <!nocheck>,
                     libmoosex-nonmoose-perl <!nocheck>,
                     libnamespace-autoclean-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libsql-translator-perl <!nocheck>,
                     libtest-dbic-expectedqueries-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-trap-perl <!nocheck>,
                     libtest-warn-perl <!nocheck>,
                     libtry-tiny-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbix-class-resultset-recursiveupdate-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbix-class-resultset-recursiveupdate-perl.git
Homepage: https://metacpan.org/release/DBIx-Class-ResultSet-RecursiveUpdate
Rules-Requires-Root: no

Package: libdbix-class-resultset-recursiveupdate-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcarp-clan-perl,
         libdata-dumper-concise-perl,
         libdbix-class-introspectablem2m-perl,
         libdbix-class-perl,
         libscalar-list-utils-perl,
         libtry-tiny-perl
Description: module for recursive updates of DBIx::Class::ResultSets
 You can feed the ->create method of DBIx::Class with a recursive
 datastructure and have the related records created. Unfortunately you cannot
 do a similar thing with update_or_create.
 .
 DBIx::Class::ResultSet::RecursiveUpdate tries to fill that void.
 .
 It is a base class for DBIx::Class::ResultSet providing the method
 recursive_update which works just like update_or_create but can recursively
 update or create result objects composed of multiple rows.
