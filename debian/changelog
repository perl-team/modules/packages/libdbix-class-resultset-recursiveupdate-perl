libdbix-class-resultset-recursiveupdate-perl (0.45-1) unstable; urgency=medium

  * Import upstream version 0.45.
  * Update years of upstream and packaging copyright.
  * Update test and runtime dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 01 Feb 2025 01:23:23 +0100

libdbix-class-resultset-recursiveupdate-perl (0.44-1) unstable; urgency=medium

  * Import upstream version 0.44.
  * Remove debian/clean, the modified test file is gone.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jul 2024 21:25:38 +0200

libdbix-class-resultset-recursiveupdate-perl (0.42-3) unstable; urgency=medium

  * Remove modified test file via debian/clean. (Closes: #1048667)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Mar 2024 16:27:04 +0100

libdbix-class-resultset-recursiveupdate-perl (0.42-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 05:51:21 +0100

libdbix-class-resultset-recursiveupdate-perl (0.42-1) unstable; urgency=medium

  * Import upstream version 0.42.

 -- gregor herrmann <gregoa@debian.org>  Thu, 27 Aug 2020 17:16:43 +0200

libdbix-class-resultset-recursiveupdate-perl (0.41-2) unstable; urgency=medium

  * Add test dependency on libtest-dbic-expectedqueries-perl.
  * Enable all tests during build and autopkgtest.

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Jun 2020 16:25:27 +0200

libdbix-class-resultset-recursiveupdate-perl (0.41-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.

  [ gregor herrmann ]
  * Import upstream version 0.41.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Add one more test to debian/tests/pkg-perl/smoke-skip.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 May 2020 00:58:39 +0200

libdbix-class-resultset-recursiveupdate-perl (0.40-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.40.
  * Add debian/upstream/metadata.
  * Update years of upstream and packaging copyright.
  * Update build and runtime dependencies.
  * Mark package as autopkgtest-able.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Drop unneeded version constraints from (build) dependencies.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Skip a new test.

 -- gregor herrmann <gregoa@debian.org>  Thu, 14 Nov 2019 19:31:24 +0100

libdbix-class-resultset-recursiveupdate-perl (0.34-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add missing dependency on liblist-moreutils-perl. (Closes: #761312)

 -- gregor herrmann <gregoa@debian.org>  Sat, 13 Sep 2014 20:29:50 +0200

libdbix-class-resultset-recursiveupdate-perl (0.34-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Feb 2014 16:02:23 +0100

libdbix-class-resultset-recursiveupdate-perl (0.31-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 25 Dec 2013 18:28:20 +0100

libdbix-class-resultset-recursiveupdate-perl (0.30-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update upstream and packaging copyright years.
  * Make build dependency on libsql-translator-perl versioned.
  * Add build dependency on libtest-exception-perl.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Sep 2013 23:48:24 +0200

libdbix-class-resultset-recursiveupdate-perl (0.25-1) unstable; urgency=low

  * New upstream release.
  * Update years of upstream copyright.
  * Add build dependencies on libtest-trap-perl, libmoose-perl, libmoosex-
    nonmoose-perl, libnamespace-autoclean-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Apr 2012 21:39:31 +0200

libdbix-class-resultset-recursiveupdate-perl (0.24-1) unstable; urgency=low

  * Initial release (closes: #590803).

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Apr 2012 22:23:09 +0200
